package utils;

public class Log {
	private static LogLevel logLevel;

	private Log() {}
	
	public static LogLevel getLogLevel() {
		return logLevel;
	}
	
	public static void setLogLevel(LogLevel logLevel) {
		Log.logLevel = logLevel;
	}

	public static void info(String who, String message) {
		if (logLevel.getLevel() >= 1) {
			System.out.printf("[INFO][%s][%s] %s\n", System.nanoTime(), who, message);
		}
	}

	public static void warning(String who, String message) {
		if (logLevel.getLevel() >= 2) {
			System.out.printf("[WARNING][%s][%s] %s\n", System.nanoTime(), who, message);
		}
	}

	public static void error(String who, String message) {
		if (logLevel.getLevel() >= 3) {
			System.out.printf("[ERROR][%s][%s] %s\n", System.nanoTime(), who, message);
		}
	}

	public static void debug(String who, String message) {
		if (logLevel.getLevel() >= 4) {
			System.out.printf("[DEBUG][%s][%s] %s\n", System.nanoTime(), who, message);
		}
	}
}

