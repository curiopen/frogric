package utils;

@FunctionalInterface
public interface Func {
	public void exec();
}

