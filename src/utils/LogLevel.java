package utils;

public enum LogLevel {
	NOLOG(0), INFO(1), WARNING(2), ERROR(3), DEBUG(4);
	private int level;
	private LogLevel(int level) {
		this.level = level;
	}
	public int getLevel() {
		return level;
	}
}

