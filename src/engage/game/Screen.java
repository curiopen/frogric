package engage.game;


import javafx.application.*;
import javafx.stage.Stage;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.input.*;
import javafx.scene.shape.*;
import javafx.scene.transform.*;
import javafx.geometry.*;
import javafx.animation.*;
import javafx.util.Duration;

import java.util.ArrayList;

import utils.*;
import engage.game.*;
import engage.game.entity.*;


public class Screen {

	private int framesExectued;
	private int lastFPS;
	private String title;
	private Func perFrame = () -> {};
	private Func perSecond = () -> {};
	protected Pane pane;
	protected Scene scene;
	protected Stage stage;
	// private Label debug;
	private AnimationTimer animaTimer;
	private Timeline secTimer;
	protected KeyHandler keyHandler;

	{
		// A timer executed per frame, used for updating the game frame
		animaTimer = new AnimationTimer() {
			@Override
			public void handle(long arg0) {
				// Count frames
				framesExectued++;
				// Exec perFrame function
				perFrame.exec();
				// Check intersection with entities (incomplete)
				// checkCollisions(p); Not needed, for now :)
			}
		};
		// animaTimer.start();
		
		// One second timer, used for measurments in second
		secTimer = new Timeline(new KeyFrame(Duration.seconds(1), e -> {
				// Save counted frames per second
				lastFPS = framesExectued;
				framesExectued = 0;
				// Exec perSecond function
				perSecond.exec();
		}));
		secTimer.setCycleCount(Animation.INDEFINITE);
		// secTimer.play();
	}

	public Screen() {
		this(new Stage());
	}
	
	public Screen(Stage stage) {
		this(stage, "Game Screen");
	}

	public Screen(Stage stage, String title) {
		this.title = title;
		this.stage = stage;
		pane = new Pane();
		scene = new Scene(pane);
		keyHandler = new KeyHandler(scene);
		stage.setScene(scene);
		stage.setTitle(title);
		// debug = new Label();
		// pane.getChildren().add(debug);

		stage.setOnShowing(e -> {
			animaTimer.start();
			secTimer.play();
		});

		stage.setOnHiding(e -> {
			animaTimer.stop();
			secTimer.stop();
		});

		stage.setOnCloseRequest(e -> {
			stage.hide();
			clear();
			animaTimer.stop();
			secTimer.stop();
			e.consume();
		});
	}

	public void start() {
		stage.show();
	}

	public void show() {
		start();
	}

	public void hide() {
		stage.hide();
		animaTimer.stop();
		secTimer.stop();
	}

	public void quit() {
		stage.close();
		clear();
		animaTimer.stop();
		secTimer.stop();
	}

	public void close() {
		quit();
	}
	
	public void clear() {
		pane.getChildren().clear();
		for (Object o : Entity.entities.toArray()) {
			Entity e = (Entity) o;
			if (e.getScreen() == this) {
				Entity.entities.remove(e);
				if (e instanceof EntityClass) EntityClass.entities.remove(e);
			}
		}
	}

	// Some getters and setters

	public int getFPS() {
		return lastFPS;
	}
	
	public Pane getPane() {
		return pane;
	}
	
	public Scene getScene() {
		return scene;
	}
	
	public KeyHandler getKeyHandler() {
		return keyHandler;
	}
	
	public double getWidth() {
		return stage.getWidth();
	}
	
	public double getHeight() {
		return stage.getHeight();
	}
	
	public void setWidth(double width) {
		stage.setWidth(width);
	}
	
	public void setHeight(double height) {
		stage.setHeight(height);
	}
	
	public void setScene() {
		stage.setScene(scene);
	}
	
	public void setScene(Scene scene) {
		stage.setScene(scene);
	}
	
	public void setTitle() {
		stage.setTitle(title);
	}

	// Function setters
	
	public void setPerFrameFunc(Func perFrame) {
		this.perFrame = perFrame;
	}
	
	public void setPerSecondFunc(Func perSecond) {
		this.perSecond = perSecond;
	}

	public void checkCollisions() {
		// This code checks where the (block) entities has intersected with our body,
		// then it stops the our body
		for (Entity e : Entity.entities) {
			for (Entity p : Entity.entities) {
				if ((p.getTags().contains("Entity") || p.getTags().contains("Player")) && p != e) {
					Bounds i = p.intersect(e).getBoundsInLocal();
					if (i.getWidth() != -1) {
						if (e.getTags().contains("Collidable")) {
							//// Check collision for every part
							// 2 || 1
							// ======
							// 3 || 4
							ArrayList<Integer> parts = new ArrayList<>();
							//// Part 1
							if (	e.getCenterX() <= p.getMaxX() && p.getMinX() <= e.getMaxX() &&
									e.getMinY() <= p.getMaxY() && p.getMinY() <= e.getCenterY()) {
								parts.add(1);
							}
							//// Part 2
							if (	e.getMinX() <= p.getMaxX() && p.getMinX() <= e.getCenterX() &&
									e.getMinY() <= p.getMaxY() && p.getMinY() <= e.getCenterY()) {
								parts.add(2);
							}
							//// Part 3
							if (	e.getMinX() <= p.getMaxX() && p.getMinX() <= e.getCenterX() &&
									e.getCenterY() <= p.getMaxY() && p.getMinY() <= e.getMaxY()) {
								parts.add(3);
							}
							//// Part 4
							if (	e.getCenterX() <= p.getMaxX() && p.getMinX() <= e.getMaxX() &&
									e.getCenterY() <= p.getMaxY() && p.getMinY() <= e.getMaxY()) {
								parts.add(4);
							}
							// if (parts.contains(1)) p.transfer(new Direction(i.getWidth(), -i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2)));
							// double width = Double.POSITIVE_INFINITY;
							// double height = Double.POSITIVE_INFINITY;
							// for (double k = 0; k <= Math.PI; k =+ 0.1) {
							// 	is.setRotate(k);
							// 	width = Math.min(width, is.getBoundsInLocal().getWidth());
							// 	height = Math.min(height, is.getBoundsInLocal().getHeight());
							// }
							while (p.intersect(e).getBoundsInLocal().getWidth() >= 0 && p.intersect(e).getBoundsInLocal().getHeight() >= 0) {
								if (parts.contains(1)) p.transfer(new Direction(i.getWidth(), -i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2)) / 16);
								if (parts.contains(2)) p.transfer(new Direction(-i.getWidth(), -i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2)) / 16);
								if (parts.contains(3)) p.transfer(new Direction(-i.getWidth(), i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2)) / 16);
								if (parts.contains(4)) p.transfer(new Direction(i.getWidth(), i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2)) / 16);
							}
						} else if (e.getTags().contains("Collidable-Line")) {
							if (p.getCenterY() <= i.getCenterY()) {
								p.transfer(0, -i.getHeight());
							} else if (p.getCenterY() > i.getCenterY()) {
								p.transfer(0, i.getHeight());
							}
						}
						if (e.getTags().contains("Ground")) {
							p.setCollidedWithGround(true);
						}
						if (e.getTags().contains("Entity")) {
							//// Check collision for every part
							// 2 || 1
							// ======
							// 3 || 4
							ArrayList<Integer> parts = new ArrayList<>();
							//// Part 1
							if (	e.getCenterX() <= p.getMaxX() && p.getMinX() <= e.getMaxX() &&
									e.getMinY() <= p.getMaxY() && p.getMinY() <= e.getCenterY()) {
								parts.add(1);
							}
							//// Part 2
							if (	e.getMinX() <= p.getMaxX() && p.getMinX() <= e.getCenterX() &&
									e.getMinY() <= p.getMaxY() && p.getMinY() <= e.getCenterY()) {
								parts.add(2);
							}
							//// Part 3
							if (	e.getMinX() <= p.getMaxX() && p.getMinX() <= e.getCenterX() &&
									e.getCenterY() <= p.getMaxY() && p.getMinY() <= e.getMaxY()) {
								parts.add(3);
							}
							//// Part 4
							if (	e.getCenterX() <= p.getMaxX() && p.getMinX() <= e.getMaxX() &&
									e.getCenterY() <= p.getMaxY() && p.getMinY() <= e.getMaxY()) {
								parts.add(4);
							}
							if (parts.contains(1)) {
								p.transfer(new Direction(i.getWidth(), -i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2))/2);
								e.transfer(new Direction(-i.getWidth(), i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2))/2);	
							}
							if (parts.contains(2)) {
								p.transfer(new Direction(-i.getWidth(), -i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2))/2);
								e.transfer(new Direction(i.getWidth(), i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2))/2);
							}
							if (parts.contains(3)) {
								p.transfer(new Direction(-i.getWidth(), i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2))/2);
								e.transfer(new Direction(i.getWidth(), -i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2))/2);
							}
							if (parts.contains(4)) {
								p.transfer(new Direction(i.getWidth(), i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2))/2);
								e.transfer(new Direction(-i.getWidth(), -i.getHeight()), Math.sqrt(Math.pow(i.getWidth(), 2)+Math.pow(i.getHeight(), 2))/2);
							}
						}
					}
				}
			}
		}
	}

	// KeyHandler Methods

	public void setKey(KeyCode k, Func f) {
		keyHandler.setKey(k, f);
	}

	public void setKeyOnPress(KeyCode k, Func f) {
		keyHandler.setKeyOnPress(k, f);
	}
	
	public void setKeyOnRelease(KeyCode k, Func f) {
		keyHandler.setKeyOnRelease(k, f);
	}

	public void removeKey(KeyCode k) {
		keyHandler.removeKey(k);
	}
}

