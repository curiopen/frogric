package engage.game.entity;

public class Direction {
	
	public static final Direction UP = new Direction(0, 1);
	public static final Direction DOWN = new Direction(0, -1);
	public static final Direction LEFT = new Direction(-1, 0);
	public static final Direction RIGHT = new Direction(1, 0);
	private double x;
	private double y;

	public Direction(double x, double y) {
		if (x == 0 && y == 0) {
			this.x = 0;
			this.y = 0;
		} else {
			this.x = x / Math.max(Math.abs(x), Math.abs(y));
			this.y = y / Math.max(Math.abs(x), Math.abs(y));
		}
	}

	public String toString() {
		return String.format("[x: %f, y: %f]", this.x, this.y);
	}

	public boolean equals(Direction d) {
		return (this.x == d.x && this.y == d.y);
	}

	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

	public Direction opposite() {
		return new Direction(-x, -y);
	}
}

