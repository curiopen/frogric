package engage.game.entity;

import javafx.scene.shape.*;
import javafx.scene.*;
import java.util.ArrayList;
import java.util.HashSet;
import engage.game.*;
import utils.Func;

public interface Entity {
	
	public static ArrayList<Entity> entities = new ArrayList<Entity>();
	
	public void move(Direction d, int size);

	public void move(Direction d);

	public void transfer(Direction d, double size);
	
	public void transfer(Direction d);

	public void transfer(double x, double y);

	public void setX(double x);
	
	public void setY(double y);

	public double getX();
	
	public double getY();
	
	public void setOnMove(Func onMove);

	public void setOnPosChange(Func onChange);
	
	public boolean getCollidedWithGround();
	
	public void setCollidedWithGround(boolean b);

	public Body getBody();

	public Screen getScreen();
	
	public HashSet<String> getTags();

	public boolean intersects(Node n);
	
	public boolean intersects(Entity e);

	public boolean intersects(double x1, double y1, double x2, double y2);
	
	public Shape intersect(Entity e);
	
	public Shape intersect(Shape s);

	public double getMinX();

	public double getMinY();

	public double getMinZ();

	public double getMaxX();

	public double getMaxY();

	public double getMaxZ();
	
	public double getCenterX();

	public double getCenterY();

	public double getCenterZ();

	public double getWidth();

	public double getHeight();

	public double getDepth();
}

