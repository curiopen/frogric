package engage.game.entity;

import javafx.scene.layout.*;
import javafx.scene.shape.*;
import javafx.scene.paint.*;
import javafx.scene.image.*;
import javafx.scene.Node;
import javafx.animation.*;
import javafx.util.Duration;

import java.util.HashSet;
import java.util.ArrayList;

import engage.game.*;
import frogric.*;
import utils.Func;

public class EntityClass implements Entity {

	public static ArrayList<Entity> entities = new ArrayList<Entity>();

	private HashSet<String> tags = new HashSet<String>();
	private Body body;
	protected Screen screen;
	protected Func onMove;
	private boolean collidedWithGround;

	public EntityClass(Screen screen) {
		this(screen, Color.BLUEVIOLET);
	}
	
	public EntityClass(Screen screen, Paint color) {
		this(screen, color, new Rectangle(40, 40));
	}
	
	public EntityClass(Screen screen, Paint color, Shape shape) {
		body = new Body(screen, shape);
		Entity.entities.add(this);
		entities.add(this);
		this.screen = screen;
		body.setFill(color);
		body.setStroke(color);
	}
	
	public EntityClass(Screen screen, Images image) {
		body = new Body(screen, image);
		Entity.entities.add(this);
		entities.add(this);
		this.screen = screen;
	}

	public void move(Direction d, int size) {
		Timeline t = new Timeline(
			new KeyFrame(Duration.millis(40/size), 
				(e) -> {
					screen.checkCollisions();
					if(onMove != null) onMove.exec();
				body.setTranslateX(body.getTranslateX()+d.getX());
				body.setTranslateY(body.getTranslateY()+d.getY());
				}
			)
		);
		t.setCycleCount(size);
		t.play();
	}

	public void move(Direction d) {
		move(d, 1);
	}
	
	public void transfer(Direction d, double size) {
		body.setTranslateX(body.getTranslateX()+d.getX()*size);
		body.setTranslateY(body.getTranslateY()+d.getY()*size);
		if(onMove != null) onMove.exec();
	}
	
	public void transfer(double x, double y) {
		body.setTranslateX(body.getTranslateX()+x);
		body.setTranslateY(body.getTranslateY()+y);
		// screen.checkCollisions();
		if(onMove != null) onMove.exec();
	}

	public void transfer(Direction d) {
		transfer(d, 1);
	}

	public void setX(double x) {
		body.setTranslateX(x);
	}
	
	public void setY(double y) {
		body.setTranslateY(y);
	}

	public double getX() {
		return body.getTranslateX();
	}
	
	public double getY() {
		return body.getTranslateY();
	}

	public void setWidth(double width) {
		if (body.getBody() instanceof Rectangle) ((Rectangle)body.getBody()).setWidth(width);
	}
	
	public void setHeight(double height) {
		if (body.getBody() instanceof Rectangle) ((Rectangle)body.getBody()).setHeight(height);
	}
	
	public void setOnMove(Func onMove) {
		this.onMove = onMove;
	}

	public void setOnPosChange(Func onChange) {
		body.boundsInParentProperty().addListener(e -> {
			onChange.exec();
		});
	}
	
	public boolean getCollidedWithGround() {
		return collidedWithGround;
	}
	
	public void setCollidedWithGround(boolean b) {
		collidedWithGround = b;
	}
	
	public void setColor(Paint p) {
		body.setFill(p);
	}
	
	public Paint getColor() {
		return body.getFill();
	}

	public Body getBody() {
		return body;
	}

	public Screen getScreen() {
		return screen;
	}

	public HashSet<String> getTags() {
		return tags;
	}

	public boolean intersects(Node n) {
		return body.getBoundsInParent().intersects(n.getBoundsInParent());
	}
	
	public boolean intersects(Entity e) {
		return body.getBoundsInParent().intersects(e.getBody().getBoundsInParent());
	}

	public boolean intersects(double x1, double y1, double x2, double y2) {
		return body.getBoundsInParent().intersects(x1, y1, x2, y2);
	}

	public Shape intersect(Shape s) {
		return this.getBody().intersect(s);
	}

	public Shape intersect(Entity e) {
		return this.getBody().intersect(e.getBody());
	}

	public double getMinX() {
		return body.getBoundsInParent().getMinX();
	}

	public double getMinY() {
		return body.getBoundsInParent().getMinY();
	}

	public double getMinZ() {
		return body.getBoundsInParent().getMinZ();
	}

	public double getMaxX() {
		return body.getBoundsInParent().getMaxX();
	}

	public double getMaxY() {
		return body.getBoundsInParent().getMaxY();
	}

	public double getMaxZ() {
		return body.getBoundsInParent().getMaxZ();
	}
	
	public double getCenterX() {
		return body.getBoundsInParent().getCenterX();
	}

	public double getCenterY() {
		return body.getBoundsInParent().getCenterY();
	}

	public double getCenterZ() {
		return body.getBoundsInParent().getCenterZ();
	}

	public double getWidth() {
		return body.getBoundsInParent().getWidth();
	}

	public double getHeight() {
		return body.getBoundsInParent().getHeight();
	}

	public double getDepth() {
		return body.getBoundsInParent().getDepth();
	}
}

