package engage.game.entity;

import javafx.scene.*;
import javafx.scene.shape.*;
import javafx.scene.paint.*;
import javafx.scene.image.*;
import javafx.geometry.*;
import javafx.beans.property.*;

import java.util.HashMap;

import engage.game.*;
import frogric.*;

public class Body {
	private static HashMap<Integer, Shape> bodyCache = new HashMap<>();
	private Shape body;
	private Images image;
	private ImageView imageView;
	private Screen screen;

	public Body(Shape... shapes) {
		boolean init = false;
		for (Shape shape : shapes) {
			if (!init) {
				body = shape;
				init = true;
			} else {
				body = Shape.union(body, shape);
			}
		}
		body.setCache(true);
		body.setCacheHint(CacheHint.SPEED);
	}
	
	public Body(Screen screen, Shape... shapes) {
		this(shapes);	
		this.screen = screen;
		this.screen.getPane().getChildren().add(body);
	}

	public Body(Images image) {
		this.image = image;
		this.imageView = new ImageView(this.image.image);
		imageView.setCache(true);
		imageView.setCacheHint(CacheHint.SPEED);
		createHitBox();
		// body.translateXProperty().bindBidirectional(imageView.translateXProperty());
		// body.translateYProperty().bindBidirectional(imageView.translateYProperty());
		// body.translateZProperty().bindBidirectional(imageView.translateZProperty());
	}
	
	public Body(Screen screen, Images image) {
		this(image);
		this.screen = screen;
		this.screen.getPane().getChildren().add(body);
		this.screen.getPane().getChildren().add(imageView);
	}

	public void createHitBox() {
		if (!bodyCache.containsKey(image.hashCode())) {
			boolean init = false;
			// boolean end = true;
			Rectangle pixels = new Rectangle();
			pixels.setCache(true);
			pixels.setCacheHint(CacheHint.SPEED);
			PixelReader r = image.hitbox.getPixelReader();
			for (int i = 0; i < image.hitbox.getWidth(); i++) {
				for (int j = 0; j < image.hitbox.getHeight(); j++) {
					if ((r.getArgb(i, j) != 0) && !Integer.toHexString(r.getArgb(i, j)).substring(0,2).equals("00")) {
						if (!init) {
							body = new Rectangle(1, 1);
							body.setCache(true);
							body.setCacheHint(CacheHint.SPEED);
							body.setLayoutX(i);
							body.setLayoutY(j);
							init = true;
						} else {
							pixels = new Rectangle(1, 1);
							pixels.setCache(true);
							pixels.setCacheHint(CacheHint.SPEED);
							pixels.setLayoutX(i);
							pixels.setLayoutY(j);
							body = Shape.union(body, pixels);	
						}
					}
				}
			}
			bodyCache.put(image.hashCode(), Shape.union(body, new Rectangle(0, 0)));
		} else {
			body = Shape.union(bodyCache.get(image.hashCode()), new Rectangle(0, 0));
		}
		body.setFill(Color.TRANSPARENT);
		body.setCache(true);
		body.setCacheHint(CacheHint.SPEED);
	}

	public void setScreen(Screen screen) {
		this.screen.getPane().getChildren().remove(body);
		if (imageView != null) this.screen.getPane().getChildren().remove(imageView);
		this.screen = screen;
		this.screen.getPane().getChildren().add(body);
		if (imageView != null) this.screen.getPane().getChildren().add(imageView);
	}

	public void remove() {
		this.screen.getPane().getChildren().remove(body);
		if (imageView != null) this.screen.getPane().getChildren().remove(imageView);
	}

	public void setImage(Images image) {
		this.image = image;
		this.imageView.setImage(image.image);
		createHitBox();
	}
	
	public void setFill(Paint paint) {
		body.setFill(paint);	
	}
	
	public void setStroke(Paint paint) {
		body.setStroke(paint);
	}

	public DoubleProperty translateXProperty() {
		return body.translateXProperty();
	}

	public DoubleProperty translateYProperty() {
		return body.translateYProperty();
	}

	public void setTranslateX(double x) {
		body.setTranslateX(x);
		if (imageView != null) imageView.setTranslateX(x);
	}

	public void setTranslateY(double y) {
		body.setTranslateY(y);
		if (imageView != null) imageView.setTranslateY(y);
	}

	public void setX(double x) {
		setTranslateX(x);
	}

	public void setY(double y) {
		setTranslateY(y);
	}

	public double getTranslateX() {
		return body.getTranslateX();
	}

	public double getTranslateY() {
		return body.getTranslateY();
	}

	public double getX() {
		return getTranslateX();
	}

	public double getY() {
		return getTranslateY();
	}

	public Shape getBody() {
		return body;
	}

	public ImageView getImageView() {
		return imageView;
	}

	public Paint getFill() { 
		if (this.image == null) return body.getFill(); else return null;
	}

	public Bounds getBoundsInParent() {
		return body.getBoundsInParent();
	}

	public Bounds getBoundsInLocal() {
		return body.getBoundsInLocal();
	}

	public Bounds getLayoutBounds() {
		return body.getLayoutBounds();
	}

	public ReadOnlyObjectProperty<Bounds> boundsInParentProperty() {
		return body.boundsInParentProperty();
	}

	public Shape intersect(Body b) {
		return Shape.intersect(this.body, b.body);
	}

	public Shape intersect(Shape s) {
		return Shape.intersect(this.body, s);
	}
}

