package engage.game;

import javafx.scene.*;
import javafx.scene.input.*;
import javafx.animation.*;
import javafx.util.Duration;
import java.util.HashSet;
import java.util.HashMap;
import utils.Func;

public class KeyHandler {

	private static HashSet<KeyCode> globalKeysInPress;
	private static boolean init;
	private final double keyRepeat = 40;
	private HashSet<KeyCode> keysInPress = new HashSet<>();
	private HashMap<KeyCode, Boolean> pressOnceFuncExecuted = new HashMap<>();
	private HashMap<KeyCode, Func> pressFunc = new HashMap<>(); 
	private HashMap<KeyCode, Func> pressOnceFunc = new HashMap<>(); 
	private HashMap<KeyCode, Func> releaseFunc = new HashMap<>(); 
	private Timeline keyrepeat;
	
	public KeyHandler(Node node) {
		node.setOnKeyPressed(e -> {
			boolean statement = keysInPress.isEmpty();
			keysInPress.add(e.getCode());
			if(statement) {
				onKeyPressed();
				keyrepeat.play();
			}
		});
		node.setOnKeyReleased(e -> {
			keysInPress.remove(e.getCode());
			onKeyReleased(e.getCode());
			if(keysInPress.isEmpty()) {
				keyrepeat.stop();
			}
		});
		keyrepeat = new Timeline(new KeyFrame(Duration.millis(keyRepeat), e -> onKeyPressed()));
		keyrepeat.setCycleCount(Animation.INDEFINITE);
		if (!init) {
			globalKeysInPress = new HashSet<>();
			init = true;
		}
	}
	
	public KeyHandler(Scene scene) {
		scene.setOnKeyPressed(e -> {
			boolean statement = keysInPress.isEmpty();
			keysInPress.add(e.getCode());
			if(statement) {
				onKeyPressed();
				keyrepeat.play();
			}
		});
		scene.setOnKeyReleased(e -> {
			keysInPress.remove(e.getCode());
			onKeyReleased(e.getCode());
			if(keysInPress.isEmpty()) {
				keyrepeat.stop();
			}
		});
		keyrepeat = new Timeline(new KeyFrame(Duration.millis(keyRepeat), e -> onKeyPressed()));
		keyrepeat.setCycleCount(Animation.INDEFINITE);
		if (!init) {
			globalKeysInPress = new HashSet<>();
			init = true;
		}
	}

	private void onKeyPressed() {
		for (KeyCode k : keysInPress) {
			pressFunc.getOrDefault(k, () -> {}).exec();
			if (!pressOnceFuncExecuted.getOrDefault(k, true))
				pressOnceFunc.get(k).exec();
				pressOnceFuncExecuted.replace(k, true);
		}
	}
	
	private void onKeyReleased(KeyCode k) {
		pressOnceFuncExecuted.replace(k, false);
		releaseFunc.getOrDefault(k, () -> {}).exec();
	}

	public void setKey(KeyCode k, Func f) {
			pressFunc.put(k, f);
	}

	public void setKeyOnPress(KeyCode k, Func f) {
			pressOnceFunc.put(k, f);
			pressOnceFuncExecuted.put(k, false);
	}
	
	public void setKeyOnRelease(KeyCode k, Func f) {
			releaseFunc.put(k, f);
	}

	public void removeKey(KeyCode k) {
		pressFunc.remove(k);
		pressOnceFunc.remove(k);
		releaseFunc.remove(k);
	}

	//// Hardcoded for now...
	// public void setKeyRepeat(int millis) {
	// 	keyrepeat.getKeyFrames().clear();
	// 	keyrepeat.getKeyFrames().add(new KeyFrame(Duration.millis(millis), e -> onKeyPressed()));
	// }

	public void setGlobal() {
		keysInPress = globalKeysInPress; 
	}
	
	public void unsetGlobal() {
		keysInPress = new HashSet<>(); 
	}

	// protected void finalize() throws Throwable {
	// 	node.setOnKeyPressed(e -> {});
	// 	node.setOnKeyReleased(e -> {});
	// }
}

