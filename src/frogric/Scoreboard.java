package frogric;

public class Scoreboard {
	private int offset = 0;
	private int score = 0;
	public Scoreboard() {}
	public void add(int i) {
		score += i;
	}
	public void add(){
		add(1);
	}
	public void rem(int i) {
		score += i;
	}
	public void rem() {
		rem(1);
	}
	public void set(int i) {
		score = i;
	}
	public int get() {
		return score;
	}
	public void setOffset(int i) {
		offset = i;
	}
	public int getOffset() {
		return offset;
	}
}

