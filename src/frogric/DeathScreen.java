package frogric;

import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.scene.shape.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.animation.*;
import javafx.util.*;

import java.io.*;

import engage.game.*;

public class DeathScreen {
	public DeathScreen(ScreenManager screen) {
		Pane pane = screen.getPane();
		Camera camera = screen.getCamera();
		Scoreboard scoreboard = screen.getScoreboard();

		Rectangle background = new Rectangle(Main.width, Main.height, Color.WHITESMOKE);
		background.setFill(new ImagePattern(Images.BackgroundPattern.image, 0, 0, 32, 32, false));
		pane.getChildren().add(background);
		pane.translateXProperty().bind(camera.translateXProperty());
		pane.translateYProperty().bind(camera.translateYProperty());
		Rectangle deathButton = new Rectangle(Main.width - 220, Main.height - 80, 200, 40);
		Label t = new Label("Restart");
		t.setTranslateX((Main.width - 220) + deathButton.getWidth()/2 - t.getWidth()/2 - 56);
		t.setTranslateY((Main.height - 80) + deathButton.getHeight()/2 - t.getHeight()/2 - 22);
		try {
			t.setFont(Font.loadFont(new FileInputStream(getClass().getResource("/resources/frogric/Quicksand-Medium.ttf").getFile()), 32));
			t.setTextFill(Color.rgb(230, 230, 230));
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(-1);
		}
		deathButton.setArcWidth(12);
		deathButton.setArcHeight(12);
		pane.getChildren().addAll(deathButton, t);

		Label scoreboardLabel = new Label("Your score is "+ Integer.toString(scoreboard.get()));
		try {
			scoreboardLabel.setFont(Font.loadFont(new FileInputStream(getClass().getResource("/resources/frogric/Quicksand-Medium.ttf").getFile()), 72));
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(-1);
		}
		scoreboardLabel.setTextAlignment(TextAlignment.LEFT);
		scoreboardLabel.setTranslateX(40);
		scoreboardLabel.setTranslateY(20);
		pane.getChildren().add(scoreboardLabel);
		
		camera.setTranslateX(0);
		camera.setTranslateY(0);
		camera.setTranslateZ(0);
		Difficulty.setLevel(1);
		
		deathButton.setOnMouseClicked(e -> {
			screen.regeneration();
			new Timeline(new KeyFrame(Duration.millis(200), ev -> {
				pane.translateXProperty().unbind();
				pane.translateYProperty().unbind();
				pane.getChildren().removeAll(background, deathButton, scoreboardLabel, t);
			})).play();
		});
		
		t.setOnMouseClicked(e -> {
			screen.regeneration();
			new Timeline(new KeyFrame(Duration.millis(200), ev -> {
				pane.translateXProperty().unbind();
				pane.translateYProperty().unbind();
				pane.getChildren().removeAll(background, deathButton, scoreboardLabel, t);
			})).play();
		});
	}
}

