package frogric;

import javafx.application.Application;
import javafx.scene.*;
import javafx.stage.Stage;
import java.io.IOException;

import utils.*;

public class Main extends Application {

	public static double width = 1024;
	public static double height = 640;
	public static double imageSize = 96;
	
	@Override
	public void start(Stage stage) throws IOException {
		stage.setResizable(false);
		ScreenManager sm = new ScreenManager(stage);
		if (Log.getLogLevel() != LogLevel.DEBUG)
			sm.start();
		else
			sm.debug();
		
		stage.setWidth(Main.width);
		stage.setHeight(Main.height);
		stage.show();
	}

	public static void main(String[] args) {
		if (args.length >= 1)
			switch (args[0]) {
				case "--info":
				case "-i":
					Log.setLogLevel(LogLevel.INFO);
					break;
				case "--warning":
				case "-w":
					Log.setLogLevel(LogLevel.WARNING);
					break;
				case "--error":
				case "-e":
					Log.setLogLevel(LogLevel.ERROR);
					break;
				case "--debug":
				case "-d":
					Log.setLogLevel(LogLevel.DEBUG);
					break;
				default:
					Log.setLogLevel(LogLevel.NOLOG);
					break;
			}
		else
			Log.setLogLevel(LogLevel.NOLOG);
		Log.setLogLevel(LogLevel.DEBUG);
		launch();
	}
}

