package frogric.surface;

import javafx.animation.*;
import javafx.event.*;
import javafx.util.Duration;

import java.util.*;
import java.util.concurrent.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.entity.*;

public abstract class Surface {
	public static CopyOnWriteArrayList<Surface> surfaces = new CopyOnWriteArrayList<Surface>();
	protected CopyOnWriteArrayList<ScreenObject> tiles = new CopyOnWriteArrayList<ScreenObject>();
	protected CopyOnWriteArrayList<Vehicle> vehicles = new CopyOnWriteArrayList<Vehicle>();
	protected CopyOnWriteArrayList<Timeline> timelines = new CopyOnWriteArrayList<Timeline>();
	protected Screen screen;
	protected int width;
	protected int y;

	public Surface(Screen screen, int y, int width) {
		this.screen = screen;
		this.y = y;
		this.width = width;
		surfaces.add(this);
	}

	public CopyOnWriteArrayList<ScreenObject> getTiles() {
		return tiles;
	}

	public CopyOnWriteArrayList<Vehicle> getVehicles() {
		return vehicles;
	}

	public int getWidth() {
		return width;
	}

	public int getY() {
		return y;
	}

	public void remove() {
		int i = 1;
		// We remove every screen object in the surface (tiles in the game surface)
		for (ScreenObject so : tiles) {
			new Timeline(new KeyFrame(Duration.millis(i), e -> {
			so.getBody().remove();
			})).play();
			i++;
			Entity.entities.remove(so);
			EntityClass.entities.remove(so);
		}
		i = 1;
		// Than we remove every vehicles in the surface
		for (Vehicle v : vehicles) {
			v.remove();
			new Timeline(new KeyFrame(Duration.millis(i), e -> {
				v.getBody().remove();
			})).play();
			i++;
			Entity.entities.remove(v);
			EntityClass.entities.remove(v);
		}
		// Stop all the related timelines
		for (Timeline tl : timelines) {
			tl.stop();
		}
		// And remove the surface from the list of surfaces
		surfaces.remove(this);
	}
}

