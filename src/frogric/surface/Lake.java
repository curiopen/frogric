package frogric.surface;

import javafx.animation.*;
import javafx.event.*;
import javafx.util.Duration;

import java.util.ArrayList;

import engage.game.*;
import frogric.entity.*;

public class Lake extends Surface {
	
	public Lake(Screen screen, int y, int width) {
		/*
			We will generated a road with it's vehicles here
		*/
		super(screen, y, width);
		// First direction is random here
		int direction = (int)(Math.random()*2);
		// Make loop for every line of the width
		for (int j = width-1; j >= 0; j--) {
			// yIndex shows where we are in the road's width
			final int yIndex = y+j;
			// A loop to add all of the tiles
			for (int i = -16; i <= 16; i++) {
				final int iFinal = i;
				new Timeline(new KeyFrame(Duration.millis(i+17), e -> {
					tiles.add(new Water(screen, iFinal, yIndex));
				})).play();
			}
			// Switch the direction every line and randomize the generation per second
			if (direction == 1) {
				direction = 0;
			} else {
				direction = 1;
			}
			final int finalDirection = direction;
			final int seconds = (int)(Math.random()*4+5);
			// This timeline generated a car for every `seconds` in this `direction`
			Timeline generationTimeline = new Timeline(new KeyFrame(Duration.seconds(seconds), e -> {
				switch (finalDirection) {
					case 0 -> {	
						RaftLeft raftLeft = new RaftLeft(screen, 16, yIndex);
						vehicles.add(raftLeft);
						raftLeft.getBody().getImageView().setViewOrder(yIndex+0.7);
					}
					case 1 -> {	
						RaftRight raftRight = new RaftRight(screen, -16, yIndex);
						vehicles.add(raftRight);
						raftRight.getBody().getImageView().setViewOrder(yIndex+0.7);
					}
					default -> {}
				}
			}));
			// We make some vehicles to populate the lake with vehicles
			ArrayList<Vehicle> lakeVehicles = new ArrayList<Vehicle>(); 
			for (int i = 1; i <= 3; i++) {
				new Timeline(new KeyFrame(Duration.millis(i), e -> {
					switch (finalDirection) {
						case 0 -> {	
							RaftLeft raftLeft = new RaftLeft(screen, 16, yIndex);
							raftLeft.getBody().getImageView().setViewOrder(yIndex+0.7);
							lakeVehicles.add(raftLeft);
							vehicles.add(raftLeft);
						}
						case 1 -> {	
							RaftRight raftRight = new RaftRight(screen, -16, yIndex);
							raftRight.getBody().getImageView().setViewOrder(yIndex+0.7);
							lakeVehicles.add(raftRight);
							vehicles.add(raftRight);
						}
						default -> {}
					}
					for (Vehicle v : lakeVehicles) {
						for (int k = 1; k <= seconds; k++) {	
							v.transfer(96);
						}
					}
				})).play();
			}
			generationTimeline.setCycleCount(Animation.INDEFINITE);
			generationTimeline.play();
			timelines.add(generationTimeline);	
		}
		// And we remove them here if they reach end of the lake
		Timeline removerTimeline = new Timeline(new KeyFrame(Duration.millis(100), e -> {
			for (Vehicle raft : vehicles) {
				if ((raft instanceof RaftRight && raft.getX() >= 16)
				||  (raft instanceof RaftLeft && raft.getX() <= -16)) {
					raft.remove();
				}
			}
		}));
		removerTimeline.setCycleCount(Animation.INDEFINITE);
		removerTimeline.play();
		timelines.add(removerTimeline);
	}
}

