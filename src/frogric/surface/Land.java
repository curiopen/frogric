package frogric.surface;

import javafx.animation.*;
import javafx.event.*;
import javafx.util.Duration;

import java.util.ArrayList;

import engage.game.*;
import frogric.entity.*;

public class Land extends Surface {
	public Land(Screen screen, int y, int width) {
		/*
			We will generated a grass land here
		*/
		super(screen, y, width);
		// Make loop for every line of the width
		for (int j = width-1; j >= 0; j--) {
			// yIndex shows where we are in the road's width
			final int yIndex = y+j;
			// A loop to add all of the tiles
			for (int i = -16; i <= 16; i++) {
				final int iFinal = i;	
				new Timeline(new KeyFrame(Duration.millis(i+17), e -> {
					tiles.add(new Grass(screen, iFinal, yIndex));
				})).play();
			}
		}
	}
}

