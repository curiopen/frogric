package frogric.surface;

import javafx.animation.*;
import javafx.event.*;
import javafx.util.Duration;

import java.util.ArrayList;

import engage.game.*;
import frogric.entity.*;
import frogric.*;

public class Road extends Surface {
	
	public Road(Screen screen, int y, int width) {
		/*
			We will generated a road with it's vehicles here
		*/
		super(screen, y, width);
		// Make loop for every line of the width
		for (int j = width-1; j >= 0; j--) {
			// yIndex shows where we are in the road's width
			final int yIndex = y+j;
			// A loop to add all of the tiles
			for (int i = -16; i <= 16; i++) {
				final int iFinal = i;
				new Timeline(new KeyFrame(Duration.millis(i+17), e -> {
					tiles.add(new Block(screen, iFinal, yIndex));
				})).play();
			}
			// We randomize the direction here and generation of vehicles per second
			final int direction = (int)(Math.random()*2);
			// This timeline generated a car for every `seconds` in this `direction`
			Timeline generationTimeline = new Timeline(new KeyFrame(Duration.seconds(Difficulty.getSecond()), e -> {
				switch (direction) {
					case 0 -> {	
						CarLeft carLeft = new CarLeft(screen, 16, yIndex);
						vehicles.add(carLeft);
						carLeft.getBody().getImageView().setViewOrder(yIndex-0.8);
					}
					case 1 -> {	
						CarRight carRight = new CarRight(screen, -16, yIndex);
						vehicles.add(carRight);
						carRight.getBody().getImageView().setViewOrder(yIndex-0.8);
					}
					default -> {}
				}
			}));
			// We make some vehicles to populate the road with vehicles
			ArrayList<Vehicle> roadVehicles = new ArrayList<Vehicle>(); 
			for (int i = 1; i <= 5; i++) {
				switch (direction) {
					case 0 -> {	
						CarLeft carLeft = new CarLeft(screen, 16, yIndex);
						carLeft.getBody().getImageView().setViewOrder(yIndex);
						roadVehicles.add(carLeft);
						vehicles.add(carLeft);
					}
					case 1 -> {	
						CarRight carRight = new CarRight(screen, -16, yIndex);
						carRight.getBody().getImageView().setViewOrder(yIndex);
						roadVehicles.add(carRight);
						vehicles.add(carRight);
					}
					default -> {}
				}
				for (Vehicle v : roadVehicles) {
					for (int k = 0; k <= Difficulty.getSecond() || k <= 3 ; k++) {	
						v.transfer(Difficulty.getMove()*25);
					}
				}
			}
			generationTimeline.setCycleCount(Animation.INDEFINITE);
			generationTimeline.play();
			timelines.add(generationTimeline);
		}
		// And we remove them here if they reach end of the road
		Timeline removerTimeline = new Timeline(new KeyFrame(Duration.millis(100), e -> {
			for (Vehicle car : vehicles) {
				if ((car instanceof CarRight && car.getX() >= 16)
				||  (car instanceof CarLeft && car.getX() <= -16)) {
					car.remove();
				}
			}
		}));
		removerTimeline.setCycleCount(Animation.INDEFINITE);
		removerTimeline.play();
		timelines.add(removerTimeline);
	}
}

