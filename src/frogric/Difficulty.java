package frogric;

public class Difficulty {
	private static int level= 1;
	public static void setLevel(int level) {
		Difficulty.level = level;
	}
	public static int getLevel() {
		return level;
	}
	public static void setLine(int line) {
		if (line < 250) {
			level = 1;
		} else if (line > 250) {
			level = 2;
		} else if (line > 500) {
			level = 3;
		} else if (line > 750) {
			level = 4;
		} else if (line > 1000) {
			level = 5;
		} else if (line > 1250) {
			level = 6;
		} else if (line > 1500) {
			level = 7;
		} else if (line > 1750) {
			level = 8;
		} else if (line > 2000) {
			level = 9;
		}	
	}
	public static int getMove() {
		return 3+level*2;
	}
	public static double getSecond() {
		return (Math.random()*(3-level/3)+(3-level/3));
	}
}

