package frogric.entity;

import javafx.scene.image.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.*;

public class Grass extends ScreenObject {
	public Grass(Screen screen, double x, double y) {
		super(screen, x, y, getImage());
	}

	private static Images getImage() {
		return switch ((int)(Math.random()*4+1)) {
			case 1:
				yield Images.Grass1;
			case 2:
				yield Images.Grass2;
			case 3:
				yield Images.Grass3;
			case 4:
				yield Images.Grass4;
			default:
				yield Images.Grass1;
		};
	}
}

