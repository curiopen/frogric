package frogric.entity;

import java.util.*;
import java.util.concurrent.*;

import engage.game.entity.*;
import engage.game.*;
import frogric.*;

public abstract class WaterVehicle extends Vehicle {

	public static CopyOnWriteArrayList<Vehicle> entities = new CopyOnWriteArrayList<Vehicle>();

	public WaterVehicle(Screen screen, double x, double y, Images image) {
		super(screen, x, y, image);
		WaterVehicle.entities.add(this);
	}

	abstract public void move();

	abstract public void move(int size);
	
	abstract public void transfer();

	abstract public void transfer(double size);

	abstract public void remove();
	
}

