package frogric.entity;

import java.util.*;
import java.util.concurrent.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.*;

public class CarLeft extends Vehicle {

	public static CopyOnWriteArrayList<Vehicle> entities = new CopyOnWriteArrayList<Vehicle>();
	
	public CarLeft(Screen screen, double x, double y) {
		super(screen, x, y, Images.CarLeft);
		CarLeft.entities.add(this);		
	}

	public void move() {
		move(Direction.LEFT);
	}

	public void move(int size) {
		move(Direction.LEFT, size);
	}

	public void transfer() {
		transfer(Direction.LEFT);
	}

	public void transfer(double size) {
		transfer(Direction.LEFT, size);
	}

	public void remove() {
		CarLeft.entities.remove(this);
		Vehicle.entities.remove(this);
		EntityClass.entities.remove(this);
		Entity.entities.remove(this);
		getBody().remove();
	}
}

