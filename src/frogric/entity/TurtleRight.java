package frogric.entity;

import java.util.*;
import java.util.concurrent.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.*;

public class TurtleRight extends WaterVehicle {

	public static CopyOnWriteArrayList<Vehicle> entities = new CopyOnWriteArrayList<Vehicle>();
	private boolean isUnderWater = false;
	
	public TurtleRight(Screen screen, double x, double y) {
		super(screen, x, y, Images.Turtle);
		TurtleRight.entities.add(this);
	}

	public void move() {
		if (Player.self.intersect(this).getBoundsInLocal().getWidth() != -1) {
			Player.self.move(Direction.RIGHT);
		}
		move(Direction.RIGHT);
	}

	public void move(int size) {
		if (Player.self.intersect(this).getBoundsInLocal().getWidth() != -1) {
			Player.self.move(Direction.RIGHT, size);
		}
		move(Direction.RIGHT, size);
	}

	
	public void transfer() {
		transfer(Direction.RIGHT);
	}
	
	public void transfer(double size) {
		transfer(Direction.RIGHT, size);
	}

	public void remove() {
		RaftRight.entities.remove(this);
		WaterVehicle.entities.remove(this);
		Vehicle.entities.remove(this);
		EntityClass.entities.remove(this);
		Entity.entities.remove(this);
		getBody().remove();
	}

	public boolean isUnderWater() {
		return isUnderWater;
	}

	public void goUp() {
		getBody().setImage(Images.TurtleUnderWater);
		isUnderWater = false;
	}

	public void goDown() {
		getBody().setImage(Images.Turtle);
		isUnderWater = true;
	}
}

