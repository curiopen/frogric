package frogric.entity;

import java.util.ArrayList;

import engage.game.*;

public class GroupTurtle {
	private ArrayList<WaterVehicle> turtles = new ArrayList<>();
	public GroupTurtle(int n, String direction, Screen screen, double x, double y) {
		for (int i = 1; i <= n; i++) {
			switch (direction) {	
				case "right":
					turtles.add(new TurtleLeft(screen, x-i, y));
					break;
				case "left":
					turtles.add(new TurtleRight(screen, x+i, y));
					break;
				default:
					try {
						throw new Exception("Turtle type undefined!");
					} catch(Exception e) {
						System.out.println(e.getMessage());
						e.printStackTrace();
						System.exit(-1);
					}
					break;
			}
		}
	}
	public void goUp() {
		for (WaterVehicle v : turtles) {
			if (v instanceof TurtleLeft) {
				((TurtleLeft)v).goUp();
			}
			if (v instanceof TurtleRight) {
				((TurtleLeft)v).goUp();
			}
		}
	}
	public void goDown() {
		for (WaterVehicle v : turtles) {
			if (v instanceof TurtleLeft) {
				((TurtleLeft)v).goDown();
			}
			if (v instanceof TurtleRight) {
				((TurtleLeft)v).goDown();
			}
		}
	}
	public void move() {
		for (WaterVehicle v : turtles) {
			if (v instanceof TurtleLeft) {
				((TurtleLeft)v).move();
			}
			if (v instanceof TurtleRight) {	
				((TurtleLeft)v).move();
			}
		}
	}
}

