package frogric.entity;

import javafx.scene.image.*;
import javafx.animation.*;
import javafx.util.*;

import engage.game.*;
import engage.game.entity.*;

import frogric.*;

public class ScreenObject extends EntityClass {
	/*
		Every Object in Game
	*/
	protected GameMap map;
	
	public ScreenObject(Screen screen, double x, double y, Images image) {
		super(screen, image);
		// Make objects go to background
		getBody().getImageView().setViewOrder(Double.MAX_VALUE);
		// Map x,y to isometric ones
		map = new GameMap(x, y);
		update();
	}

	public GameMap getMap() {
		return map;
	}

	public void update() {
		super.setX(map.getX());
		super.setY(map.getY());
	}

	@Override
	public void move(Direction d, int size) {
		Timeline t = new Timeline(
			new KeyFrame(Duration.millis(40.0/size), 
				(e) -> {
					// screen.checkCollisions(); // Disable collision check for more speed!
					if(onMove != null) onMove.exec();
					// Divide on Image Size to move per pixel instead of per grid
					setX(getX()+d.getX()/Main.imageSize);
					setY(getY()+d.getY()/Main.imageSize);
				}
			)
		);
		t.setCycleCount(size);
		t.play();
	}
	
	@Override
	public void transfer(Direction d, double size) {
		// Divide on Image Size to move per pixel instead of per grid
		setX(getX()+(d.getX()/Main.imageSize)*size);
		setY(getY()+(d.getY()/Main.imageSize)*size);
		if(onMove != null) onMove.exec();
	}
	
	@Override
	public void transfer(double x, double y) {
		// Divide on Image Size to move per pixel instead of per grid
		setX(getX()+x/Main.imageSize);
		setY(getY()+y/Main.imageSize);
		if(onMove != null) onMove.exec();
	}

	@Override
	public void setX(double x) {
		map.setIsoX(x);
		update();
	}

	@Override
	public void setY(double y) {
		map.setIsoY(y);
		update();
	}

	@Override
	public double getX() {
		return map.getIsoX();
	}

	@Override
	public double getY() {
		return map.getIsoY();
	}
}

