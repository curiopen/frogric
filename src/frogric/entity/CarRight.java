package frogric.entity;

import java.util.*;
import java.util.concurrent.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.*;

public class CarRight extends Vehicle {

	public static CopyOnWriteArrayList<Vehicle> entities = new CopyOnWriteArrayList<Vehicle>();
	
	public CarRight(Screen screen, double x, double y) {
		super(screen, x, y, Images.CarRight);
		CarRight.entities.add(this);
	}

	public void move() {
		move(Direction.RIGHT);
	}

	public void move(int size) {
		move(Direction.RIGHT, size);
	}
	
	public void transfer() {
		transfer(Direction.RIGHT);
	}
	
	public void transfer(double size) {
		transfer(Direction.RIGHT, size);
	}
	
	public void remove() {
		CarRight.entities.remove(this);
		Vehicle.entities.remove(this);
		EntityClass.entities.remove(this);
		Entity.entities.remove(this);
		getBody().remove();
	}
}

