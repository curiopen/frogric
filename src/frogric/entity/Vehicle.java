package frogric.entity;

import java.util.*;
import java.util.concurrent.*;

import engage.game.*;
import frogric.*;

public abstract class Vehicle extends ScreenObject {

	public static CopyOnWriteArrayList<Vehicle> entities = new CopyOnWriteArrayList<Vehicle>();

	public Vehicle(Screen screen, double x, double y, Images image) {
		super(screen, x, y, image);
		Vehicle.entities.add(this);
	}

	abstract public void move();
	
	abstract public void move(int size);
	
	abstract public void transfer();

	abstract public void transfer(double size);
	
	abstract public void remove();
}

