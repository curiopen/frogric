package frogric.entity;

import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.animation.*;
import javafx.util.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.*;

public class Player extends ScreenObject {
	public static Player self;
	
	private Player(Screen screen, double x, double y) {
		super(screen, x, y, Images.Player);
		moveInit();
		getBody().getImageView().setViewOrder(getY());
	}

	public static void init(Screen screen, double x, double y) {
		Player.self = new Player(screen, x, y);
	}

	private void moveInit() {
		screen.getKeyHandler().setKey(KeyCode.D, () -> {
			move(Direction.RIGHT, 16);
			getBody().getImageView().setViewOrder(getY());
		});
		screen.getKeyHandler().setKey(KeyCode.A, () -> {
			move(Direction.LEFT, 16);
			getBody().getImageView().setViewOrder(getY());
		});
		screen.getKeyHandler().setKey(KeyCode.W, () -> {
			getBody().getImageView().setViewOrder(getY()+16/96.0);
			move(Direction.UP, 16);
		});
		screen.getKeyHandler().setKey(KeyCode.S, () -> {
			getBody().getImageView().setViewOrder(getY()-16/96.0);
			move(Direction.DOWN, 16);
		});
	}
}

