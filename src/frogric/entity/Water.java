package frogric.entity;

import javafx.scene.image.*;

import java.util.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.*;

public class Water extends ScreenObject {
	public static ArrayList<Water> blocks = new ArrayList<Water>();
	
	public Water(Screen screen, double x, double y) {
		super(screen, x, y, Images.Water);
		blocks.add(this);
	}
}

