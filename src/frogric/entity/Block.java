package frogric.entity;

import javafx.scene.image.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.*;

public class Block extends ScreenObject {
	public Block(Screen screen, double x, double y) {
		super(screen, x, y, Images.Block);
		getTags().add("Collidable");
	}
}

