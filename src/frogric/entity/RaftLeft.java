package frogric.entity;

import java.util.*;
import java.util.concurrent.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.*;

public class RaftLeft extends WaterVehicle {

	public static CopyOnWriteArrayList<Vehicle> entities = new CopyOnWriteArrayList<Vehicle>();
	
	public RaftLeft(Screen screen, double x, double y) {
		super(screen, x, y, Images.RaftRight);
		RaftLeft.entities.add(this);
	}

	public void move() {
		if (Player.self.intersect(this).getBoundsInLocal().getWidth() != -1) {
			Player.self.move(Direction.LEFT);
		}
		move(Direction.LEFT);
	}

	public void move(int size) {
		if (Player.self.intersect(this).getBoundsInLocal().getWidth() != -1) {
			Player.self.move(Direction.LEFT, size);
		}
		move(Direction.LEFT, size);
	}

	
	public void transfer() {
		transfer(Direction.LEFT);
	}
	
	public void transfer(double size) {
		transfer(Direction.LEFT, size);
	}

	public void remove() {
		RaftRight.entities.remove(this);
		WaterVehicle.entities.remove(this);
		Vehicle.entities.remove(this);
		EntityClass.entities.remove(this);
		Entity.entities.remove(this);
		getBody().remove();
	}
}

