package frogric;

public class GameMap {
	/*
		This class is desigen to convert xy coordinations
		into isometric xy coordinations
	*/
	private final double imageSize = Main.imageSize;
	private final double isoXConst = imageSize/2;
	private final double isoYConst = imageSize/4;
	private double isoX;
	private double isoY;
	public GameMap(double x, double y) {
		this.isoX = x;
		this.isoY = y;
	}
	
	public double getX() {
		return (isoX) * isoXConst + (isoY) * isoXConst - isoXConst;
	}
	
	public double getY() {
		return (isoX) * isoYConst + -(isoY) * isoYConst - (isoXConst)*2 + Main.height;
	}

	public void setXY(double x, double y) {
		this.isoX = (x / (2*isoXConst)) + ((y+2*isoXConst-Main.height)/(2*isoYConst)) + 1/2;
		this.isoY = (x / (2*isoXConst)) + ((-y-2*isoXConst+Main.height)/(2*isoYConst)) + 1/2;
	}

	public void setX(double x) {
		setXY(x, getY());
	}

	public void setY(double y) {
		setXY(getX(), y);
	}
	
	public void setIsoX(double x) {
		this.isoX = x;
	}
	
	public void setIsoY(double y) {
		this.isoY = y;
	}

	public double getIsoX() {
		return isoX;
	}
	
	public double getIsoY() {
		return isoY;
	}
}

