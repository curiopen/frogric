package frogric;

import javafx.stage.Stage;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.shape.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.animation.*;
import javafx.util.*;

import java.util.*;
import java.io.*;

import engage.game.*;
import engage.game.entity.*;
import frogric.entity.*;
import frogric.surface.*;
import utils.*;

public class ScreenManager extends Screen {
	/*
		Manages every things that happens on the screen
	*/
	private ArrayList<Timeline> timelines = new ArrayList<Timeline>();
	private int generatedLines = 0; 
	private int removedLines = -1;
	private Rectangle screenHitBox;
	private Camera camera;
	private Player player;
	private Scoreboard scoreboard;
	private boolean generating = false;
	private boolean godMode = false;

	public ScreenManager(Stage stage) {
		super(stage, "Frogric");
		this.camera = new PerspectiveCamera();
		// Pane deathPane = new Pane();
		// Button deathButton = new Button("Restart");
		// deathButton.setOnAction(e -> stage.setScene(scene));
		// deathPane.getChildren().add(deathButton);
		// this.deathScene = new Scene(deathPane);
	}

	public void start() {
		/*
			Starts the game
		*/
		initCamera();
		addSurface();
		addPlayer();
		addScoreboard();

		// This timeline moves the camera when the frog reaches top-right corner of the screen
		Timeline cameraTimeline = new Timeline(new KeyFrame(Duration.millis(40), e -> {
			// if (player.getMap().getX()-camera.getTranslateX() > Main.width/3 && player.getMap().getY()-camera.getTranslateY() < Main.height/3) {
			if (-(player.getMap().getY()-camera.getTranslateY()) >= (-(Main.height/Main.width)*(player.getMap().getX()-camera.getTranslateX()) + 32)) {
				Timeline tl = new Timeline(new KeyFrame(Duration.millis(40/8), ev -> {
					camera.setTranslateX(camera.getTranslateX()+2);
					camera.setTranslateY(camera.getTranslateY()-1);
					scene.setCamera(camera);
				}));
				tl.setCycleCount(8);
				tl.play();
			}			
		}));
		cameraTimeline.setCycleCount(Animation.INDEFINITE);
		cameraTimeline.play();
		timelines.add(cameraTimeline);

		// This timeline moves the camera when the frog reaches bounds of screen
		Timeline blockerTimeline = new Timeline(new KeyFrame(Duration.millis(1), e -> {
			// Don't let the player go out of the screen
			screenHitBox.setTranslateX(camera.getTranslateX());
			screenHitBox.setTranslateY(camera.getTranslateY());
			if (!player.intersects(screenHitBox)) {
				deathScreen();
			}
		}));
		blockerTimeline.setCycleCount(Animation.INDEFINITE);
		blockerTimeline.play();
		timelines.add(blockerTimeline);
		
		// This timeline generates new surfaces and removes the old one
		Timeline generationTimeline = new Timeline(new KeyFrame(Duration.millis(100), e -> {
			if (player.getY() >= generatedLines-15) {
				if (generating == false) {
					Log.info("ScreenManager/generationTimeline", "Generating Surface");
					generating = true;
					int type = -1;
					int i = generatedLines+1;
					while (true) {
						if (type == -1) {
							int r = (int)(Math.random()*2+3);
							new Land(this, i, r);
							i += r-1;
						} else if (type == 0) {	
							int r = (int)(Math.random()*3+2);
							new Road(this, i, r);
							i += r-1;
						} else if (type == 1) {	
							int r = (int)(Math.random()*3+3);
							new Lake(this, i, r);
							i += r-1;
						}
						if (type == -1) {
							type = (int)(Math.random()*2);
						} else {
							generatedLines = i;
							break;
						}
					}
					generating = false;
					Log.info("ScreenManager/generationTimeline", "Surface generated");
				}
			}
			// Removing old surfaces
			for (Surface s : Surface.surfaces) {
				if (s.getY()+s.getWidth()-1 <= player.getY()-15) {
					Log.info("ScreenManager/generationTimeline", "Removing surface");
					Surface.surfaces.remove(s);
					removedLines = s.getY()+s.getWidth()-1;
					s.remove();
					Log.info("ScreenManager/generationTimeline", "Surface removed");
				}	
			}
		}));
		generationTimeline.setCycleCount(Animation.INDEFINITE);
		generationTimeline.play();
		timelines.add(generationTimeline);
		
		// And this timeline simply moves the vehicles
		Timeline animationTimeline = new Timeline(new KeyFrame(Duration.millis(40), e -> {
			for (Vehicle v : Vehicle.entities) {
				v.move(Difficulty.getMove());
				// Difficulty.setLine(generatedLines);
			}
		}));
		animationTimeline.setCycleCount(Animation.INDEFINITE);
		animationTimeline.play();
		timelines.add(animationTimeline);
		
		// This timeline check if any death collsion happens and does the coresponding action
		Timeline collisionTimeline = new Timeline(new KeyFrame(Duration.millis(100), e -> {
			// Set score base on player Y position
			scoreboard.set(Math.max((int)player.getY(), scoreboard.get()));
			for (Surface s : Surface.surfaces) {
				if (s.getY() <= player.getY()+1 && player.getY()-1 <= s.getY()+s.getWidth()-1) {
					if (s instanceof Lake) {
						boolean safeOnWater = false;
						for (ScreenObject w : s.getTiles()) {
							if (player.intersect(w).getBoundsInLocal().getWidth() >= player.getWidth()-Main.imageSize/8.0) {
								for (Vehicle v : s.getVehicles()) {
									if (player.intersect(v).getBoundsInLocal().getWidth() != -1) {
										safeOnWater = true;
										// Log.info("ScreenManager/collisionTimeline", "Collision with raft detected");
									}
								}
								if (!safeOnWater) {
									deathScreen();
									Log.info("ScreenManager/collisionTimeline", "Drowning");
								} else {
								}
								safeOnWater = false;
							}
						}
					} else if (s instanceof Road) {
						for (Vehicle v : s.getVehicles()) {
							if (player.intersect(v).getBoundsInLocal().getWidth() != -1) {
								deathScreen();
								Log.info("ScreenManager/collisionTimeline", "Collision with vehicle");
							}
						}
					}
				}
			}
		}));
		collisionTimeline.setCycleCount(Animation.INDEFINITE);
		collisionTimeline.play();
		timelines.add(collisionTimeline);
	}

	public void deathScreen() {
		if (godMode)
			return;
		if (generating == true) return;
		generating = true;
		player.setX(0);
		player.setY(1);
		Log.info("ScreenManager/deathScreen", "Start deathScreen");	
		for (Timeline t : timelines) {
			t.stop();
		}
		player.getBody().getImageView().setViewOrder(1);
		
		new DeathScreen(this);
	}

	public void regeneration() {
		/*
			Rengenerates the map after death
		*/
		// stage.setScene(deathScene);
		Log.info("ScreenManager/regeneration", "Start regenerating");	
		for (Surface s: Surface.surfaces)
			s.remove();
		generatedLines = 40;
		removedLines = -1;
		addSurface();
		player.setX(0);
		player.setY(1);
		scoreboard.set(0);
		player.getBody().getImageView().setViewOrder(1);
		camera.setTranslateX(0);
		camera.setTranslateY(0);
		camera.setTranslateZ(0);
		scene.setCamera(camera);
		for (Timeline t : timelines)
			t.playFromStart();
		Log.info("ScreenManager/regeneration", "Regenerating finished");	
		generating = false;
	}

	public void debug() {
		/*
			For Debugging purposes
		*/
		start();
		// Chagne difficulty level
		keyHandler.setKeyOnPress(KeyCode.EQUALS, () -> {
			Difficulty.setLevel(Difficulty.getLevel()+1);	
			Log.info("ScreenManage/debug", "Difficulty level increased to "+Difficulty.getLevel());	
		});
		keyHandler.setKeyOnPress(KeyCode.MINUS, () -> {
			Difficulty.setLevel(Difficulty.getLevel()-1);	
			Log.info("ScreenManage/debug", "Difficulty level decreased to "+Difficulty.getLevel());	
		});
		// Activate God Mode with F1
		keyHandler.setKeyOnPress(KeyCode.F1, () -> {
			godMode = !godMode;
			if (godMode)
				Log.info("ScreenManage/debug", "God Mode Actiavted");	
			else
				Log.info("ScreenManage/debug", "God Mode Deactiavted");	
		});
		// Add movement keys to camera
		keyHandler.setKey(KeyCode.UP, () -> {
			camera.setTranslateY(camera.getTranslateY()-10);
			scene.setCamera(camera);
		});
		keyHandler.setKey(KeyCode.DOWN, () -> {
			camera.setTranslateY(camera.getTranslateY()+10);
			scene.setCamera(camera);
		});
		keyHandler.setKey(KeyCode.LEFT, () -> {
			camera.setTranslateX(camera.getTranslateX()-10);
			scene.setCamera(camera);
		});
		keyHandler.setKey(KeyCode.RIGHT, () -> {
			camera.setTranslateX(camera.getTranslateX()+10);
			scene.setCamera(camera);
		});
		keyHandler.setKey(KeyCode.PAGE_UP, () -> {
			camera.setTranslateZ(camera.getTranslateZ()+10);
			scene.setCamera(camera);
		});
		keyHandler.setKey(KeyCode.PAGE_DOWN, () -> {
			camera.setTranslateZ(camera.getTranslateZ()-10);
			scene.setCamera(camera);
		});
	}

	public void addSurface() {
		/*
			This method add initial surfaces to the map
		*/
		Log.info("ScreenManager/addSurface", "Generating Surface");
		int type = 0;
		int i = 0;
		while (i <= 25) {
			if (type == 0) {
				int r = (int)(Math.random()*2+3);
				new Land(this, i, r);
				i += r;
				type = (int)(Math.random()*2) == 0 ? -1 : 1;
			} else if (type == 1) {	
				int r = (int)(Math.random()*3+2);
				new Road(this, i, r);
				i += r;
				type = 0;
			} else if (type == -1) {	
				int r = (int)(Math.random()*3+3);
				new Lake(this, i, r);
				i += r;
				type = 0;
			}
		}
		generatedLines = i - 1;
		Log.info("ScreenManager/addSurface", "Surface Generated");
	}

	public void addPlayer() {
		/*
			Simply adds the player
		*/
		Log.info("ScreenManager/addPlayer", "Adding the player");
		Player.init(this, 0, 1);
		this.player = Player.self;
		Log.info("ScreenManager/addPlayer", "Player added");
	}

	public void addScoreboard() {
		/*
			Adds scoreboard
		*/
		scoreboard = new Scoreboard();
		scoreboard.setOffset(-1);
		// try {
		// 	scoreboard.setFont(Font.loadFont(new FileInputStream(getClass().getResource("/resources/frogric/Quicksand-Medium.ttf").getFile()), 72));
		// } catch(FileNotFoundException e) {
		// 	System.out.println(e.getMessage());
		// 	e.printStackTrace();
		// 	System.exit(-1);
		// }
		// scoreboard.setStyle("-fx-fill-color: white;");
	}

	public void initCamera() {
		camera.setTranslateX(0);
		camera.setTranslateY(0);
		camera.setLayoutX(0);
		camera.setLayoutY(0);
		scene.setCamera(camera);
		screenHitBox = new Rectangle(camera.getTranslateX(), camera.getTranslateY(), Main.width+1, Main.height);
		screenHitBox.setFill(Color.TRANSPARENT);
		pane.getChildren().add(screenHitBox);
	}

	public Camera getCamera() {
		return camera;
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}
}

