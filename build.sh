#!/usr/bin/env bash

for codejava in ./src/*/*
do
	if [[ $codejava == *.java ]]
	then
		javac --module-path ./lib --add-modules ALL-MODULE-PATH -cp ./src/ -d ./classes $codejava
	fi
done

rm -rf ./classes/resources
cp -r ./src/resources ./classes/resources

